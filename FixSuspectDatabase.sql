USE master
DECLARE @DB VARCHAR(30)
SET @DB = '[Your_DataBase_Name]';

--Put the Database into Emergency Mode
Exec ('ALTER DATABASE'+ @DB +'SET EMERGENCY');

--Check the Database for consistency
Exec ('DBCC CHECKDB ('+ @DB +')');

--Put the Database into Single User Mode
Exec ('ALTER DATABASE'+ @DB + 'SET SINGLE_USER');

--Repair the Database
Exec ('DBCC CHECKDB(' + @DB +', REPAIR_ALLOW_DATA_LOSS)');

--Put the Database back into multiuser mode
Exec ('ALTER DATABASE' + @DB + 'SET MULTI_USER');

--Put the Databse online if it is not already
Exec ('ALTER DATABASE' + @DB + 'SET ONLINE');

